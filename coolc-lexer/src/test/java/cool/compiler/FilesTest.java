package cool.compiler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import au.com.bytecode.opencsv.CSVReader;
import cool.compiler.autogen.lexer.LexerException;

public class FilesTest {

	@DataProvider(name = "filesProvider")
	public Iterator<Object[]> readCases() throws IOException {
		CSVReader reader = new CSVReader(new FileReader(
				"src/test/resources/cases"), ';');
		ArrayList<Object[]> list = new ArrayList<Object[]>();

		String[] nextLine;
		while ((nextLine = reader.readNext()) != null) {
			list.add( new Object[] { nextLine[0], nextLine[2] } );
		}

		return list.iterator();
	}

	@Test(dataProvider = "filesProvider")
	public void test(String file, String testName) throws LexerException, IOException {
		PrintStream out = new PrintStream(new FileOutputStream("src/test/resources/output/" + file + ".out"));
		new Main().lexerCheck("src/test/resources/input/" + file, out);
		
		File reference = new File("src/test/resources/reference/" + file + ".out");
		Iterator<String> refLines = FileUtils.readLines(reference).iterator();
		Iterator<String> outLines = FileUtils.readLines(new File("src/test/resources/output/" + file + ".out")).iterator();
		
		while(true) {				
			if (!refLines.hasNext()) break;
			String r = refLines.next();
			String o = outLines.next();
			assert r.compareTo(o) == 0 : String.format("%s -> %s, reference=[%s], output=[%s]", file, testName, r, o);
		}
	}

}
