package cool.compiler;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PushbackReader;

import cool.compiler.autogen.lexer.Lexer;
import cool.compiler.autogen.lexer.LexerException;
import cool.compiler.autogen.node.EOF;
import cool.compiler.autogen.node.Token;

public class Main {
	public static void main(String [] args) throws LexerException, IOException {
		new Main().lexerCheck("src/test/resources/input/all_else_true.cl.cool", System.out);
	}
	
	public void lexerCheck(String file, PrintStream out) throws LexerException, IOException {
		Lexer lexer = new Lexer(new PushbackReader(new FileReader(file)));
		
		out.format("#name %s\n", file);
		while(true) {
			Token token = lexer.next();
			
			if (token instanceof EOF) {
				break;
			}			
			
			if (!token.getClass().getSimpleName().equals("TBlanks")){
				out.format("#%d %s \"%s\"\n", 
						token.getLine(),
						token.getClass().getSimpleName(),
						Util.escapeString(token.getText()));
			}
			
					
		}

	}

}
