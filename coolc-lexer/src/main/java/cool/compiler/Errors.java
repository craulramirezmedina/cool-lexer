package cool.compiler;

public class Errors {
	public static String errGral = "#%d ERROR \"%s\".\n";
	public static String errStrEol = "#%d ERROR Unterminated string constant, EOL found.\n";	
	public static String errStrLongConst = "#%d ERROR String constant too long.\n";	
	public static String errNull = "#%d ERROR String contains null character.\n";	
	public static String errStrEscapedNull = "#%d ERROR String contains escaped null character.\n";
	public static String errCommentUnmatched = "#%d ERROR Unmatched *).\n";
	public static String errStrEof = "#%d ERROR EOF in string constant.\n";
	public static String errCommentEof = "#%d ERROR EOF in comment.\n";
	
	

}
